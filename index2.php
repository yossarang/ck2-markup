<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="expires" content="-1" />
<meta name="copyright"content="Cheongkkae">
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>청깨닷컴</title>
<link rel="stylesheet" href="/css/fonts.css" type="text/css" />
<link rel="stylesheet" href="/css/common.css" type="text/css" />
<link rel="stylesheet" href="/css/main.css" type="text/css" />
<link rel="stylesheet" href="/css/jquery.mCustomScrollbar.css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

<!------------------ script -------------------------------->
<script type="text/javascript" src="/js/jquery-latest.js"></script>
<script type='text/javascript' src="/js/jQueryRotate.js"></script> 
<script type='text/javascript' src="/js/jquery_color/jquery.xcolor.js"></script>
<script type='text/javascript' src="/js/jquery_color/jquery.xcolor.min.js"></script>
<script type="text/javascript" src="/js/easing.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<script type="text/javascript" src="/js/nav.js"></script>
<script type="text/javascript" src="/js/jquery.mCustomScrollbar.concat.min.js"></script>
</head>

<body>

<!---------------------------- 상단영역 ------------------------------->
 <section id="head_wrap">
   <div class="head_top_wrap">
     <div class="notice_wrap">
       <ul>
          <li>NOTICE</li>
          <li>
            <ul>
             <li>청깨닷컴이 새롭게 단장했습니다.</li>
             <li>청깨닷컴에 도움을 주실 분을 찾습니다.</li>
             <li>청깨닷컴 임시점검 안내</li>
            </ul>
          </li>
          <li><img src="img/main/notice_arrow_up.png" alt="버튼"/></li>
          <li><img src="img/main/notice_arrow_down.png" alt="버튼"/></li>
       </ul>
     </div>
     
     <img src="img/main/logo.jpg" alt="로고"/>
    
   </div>
   
   <div class="line_wrap">
     <div class="line"></div>
   </div>
   
   <div class="nav_wrap">
      <ul>
       <!-- 청깨소개  -->
        <li>
         청깨소개
          <ul>
            <li></li>
            <li></li>
          </ul>
        <li>
      </ul>
   </div>
 </section>
 
</body>
</html>