// JavaScript Document
$(document).ready(function(){
	navset();
	navwork();
	setnotice();
	ntcmouseWork();
	resizeBanner();
	mobileWork();
	mobileMenuSpread();
	$(function() {
 
      $( "#datepicker" ).datepicker();
 
   });
   
	$('.maximage').maximage();
	
	
	$(function(){
		$('.maximage').maximage({
			horizontalCenter: true,
			verticalCenter: true,
			fillElement: '.holder',
			backgroundSize: 'contain'
		});
   });
   
   movie_raito()
});


function log(value){
	window.console&&console.log(value);
}

// 윈도우 가로 세로 사이즈
function brw(){
	var brw = $(window).width();
	return brw;
}

function brh(){
	var brh = $(window).height();
	return brh;
}





/*------------------------------------------------------------------------------------------------------------------

# 상단 네비게이션 

--------------------------------------------------------------------------------------------------------------------*/



//네비게이션 세팅
function navset(){
	var marginleft;
	var totalw = 0;
	
   navset_inner();
   
   
   $(window).resize(function(){
	   navset_inner();
   });
   
   function navset_inner(){
	   var totalw = 0;
	   marginleft = 0;
	   var totalnum = $('.nav_wrap > ul > li').length;//메뉴 개수
	   $('.nav_wrap > ul > li').each(function(){
		   var idx = $(this).index();
		   
		   var getW = 1155; //화면 가로 길이 값
		   if(brw() < 1170){ getW = (brw()-30)}
		   
		   totalw  = totalw + $(this).width(); //각 메뉴별 길이값을 모두 더한 값
		   if(idx >= totalnum-1){ 
		      marginleft = (getW - totalw)/(totalnum-1); //(화면길이 - 메뉴길이)/메뉴개수-1

			  $('.nav_wrap > ul > li').css({"margin-left":marginleft})
			  $('.nav_wrap > ul > li').eq(0).css({"margin-left":15})
		   }
		    
	   });
	  
	  var copyW_arr = []; 
	 $('.line_wrap > ul > li').each(function(){
	   var idx = $(this).index();
	   var copyW = $('.nav_wrap > ul > li').eq(idx).width(); //막대의 각 길이를 메뉴의 길이와 같게 맞춘다.
	   copyW_arr[idx] = copyW;
	   var totalnum = $('.line_wrap > ul > li').length;

	   if(idx == 0){
		    $(this).css({"margin-left":15}) 
		   }else{
		    $(this).css({'margin-left':marginleft+copyW_arr[idx-1]}); //막대의 마진값은 메뉴의 마진값과 같다.
			//log(marginleft)
			//log(copyW_arr[idx-1])
	   }
    });
	   
   }
}



//네비게이션 마우스 컨트롤
function navwork(){
	$('.nav_wrap > ul > li > ul > li').mouseover(function(){
		var idx=$(this).index();
		$(this).stop().animate({"color":"#f26e6e"},500);
	});
	$('.nav_wrap > ul > li').mouseover(function(){
		clearInterval(navtimeset);
		var idx=$(this).index();
		var stick = $('.line_wrap > ul > li').eq(idx);
		var thisw = $(this).width();
		stick.stop().animate({"width":thisw},500,"easeInOutExpo");
		$('.nav_wrap').stop().animate({"height":472},500,"easeInOutExpo");
	});
	
	var navtimeset;
	
	$('.nav_wrap > ul > li').mouseleave(function(){
		var idx=$(this).index();
		var stick = $('.line_wrap > ul > li').eq(idx);
		var thisw = $(this).width();
		stick.stop().animate({"width":0});
		navtimeset = setInterval(navback,700);
	});
	
	function navback(){
		$('.nav_wrap').stop().animate({"height":52},500,"easeInOutExpo",function(){clearInterval(navtimeset)});
	}
}




/*------------------------------------------------------------------------------------------------------------------

# 공지사항

--------------------------------------------------------------------------------------------------------------------*/

var ntc_timer = setInterval(ntcwork,3000);
var ntc_idx = 0;
var past_ntc = 0;
var count = 0;
function ntcwork(){
	ntc_idx++;
	if(ntc_idx > 2){ntc_idx = 0}
	$('.nt_cont_wrap > li').eq(ntc_idx).stop().animate({"top":0})
	$('.nt_cont_wrap > li').eq(past_ntc).stop().animate({"top":-25},function(){$(this).css({"top":25*3})});
	past_ntc = ntc_idx;
	count++;
}


function ntcwork2(){
	ntc_idx--;
	if(ntc_idx < 0){ntc_idx = 2}
	$('.nt_cont_wrap > li').eq(ntc_idx).stop().animate({"top":0})
	$('.nt_cont_wrap > li').eq(past_ntc).stop().animate({"top":25},function(){$(this).css({"top":-25*3})});
	past_ntc = ntc_idx;
	count++;
}



function setnotice(){
	$('.nt_cont_wrap > li').each(function(){
		var idx = $(this).index();
		$(this).css({"top":idx*25});
	});
}


function ntcmouseWork(){
	$(".nt_arrow_down").mouseover(function(){
		clearInterval(ntc_timer);
	});
	
	$(".nt_arrow_down").mouseleave(function(){
		ntc_timer = setInterval(ntcwork,3000);
	});
	
	$(".nt_arrow_up").mouseover(function(){
		clearInterval(ntc_timer);
	});
	
	$(".nt_arrow_up").mouseleave(function(){
		ntc_timer = setInterval(ntcwork,3000);
	});
	
	$(".nt_arrow_down").click(function(){
		ntcwork();
	});
	
	$(".nt_arrow_up").click(function(){
		ntcwork2();
	});
}




/*------------------------------------------------------------------------------------------------------------------

# 메인 배너 리사이즈

--------------------------------------------------------------------------------------------------------------------*/

function raitoH(setwidth,setheight,noww){
	var w = setwidth;
	var h = setheight;
	
	var percent = h/(w/100);
	var newh = (noww/100)*percent;
	return newh;
}

function resizeBanner(){

	$(window).resize(function(){
		sizeset();
	});
	
	sizeset();
	function sizeset(){
		
		if(brw() > 640){
	        $("#main_banner").css({"height":raitoH(1900,870,brw())})
		}else{
			$("#main_banner").css({"height":raitoH(1900,870,640)})
		}
	}
}




/*------------------------------------------------------------------------------------------------------------------

# 모바일 메뉴 

--------------------------------------------------------------------------------------------------------------------*/

function mobileWork(){
	var toggle = false;
	$('.swapicon_wrap').click(function(){
		if(!toggle){
		 $('.swapicon_wrap > div').eq(0).css({"display":"none"});
		 $('.swapicon_wrap > div').eq(1).stop().rotate({animateTo:-45});
		 $('.swapicon_wrap > div').eq(2).stop().rotate({animateTo:45});
		 $('.swapicon_wrap > div').eq(3).css({"display":"none"});
		 m_menuon();
		 toggle = true;
		}else{
		 $('.swapicon_wrap > div').eq(0).css({"display":"block"});
		 $('.swapicon_wrap > div').eq(1).stop().rotate({animateTo:0});
		 $('.swapicon_wrap > div').eq(2).stop().rotate({animateTo:0});
		 $('.swapicon_wrap > div').eq(3).css({"display":"block"});
		 toggle = false;
		 m_menuoff();
		}
	});
	
	
	function m_menuon(){
		$('.m_menu_wrap').css({"display":"block"});
		
		var m_time = setInterval(mlion,200);
		var idx = 0;
		var totalnum = $('.m_menu_wrap > ul > li').length;
		function mlion(){
			$('.m_menu_wrap > ul > li').eq(idx).addClass('on');
			idx++
			if(idx >= totalnum){clearInterval(m_time)}
		}
	}
	
	function m_menuoff(){
		$('.m_menu_wrap').css({"display":"none"});
		$('.m_menu_wrap > ul > li').removeClass();
		
	}
}


function mobileMenuSpread(){
	var totalnum = $('.m_menu_wrap > ul > li').size();
    var toggle_arr = [];
		for(i=0; i<totalnum; i++){
		 //var idx = $(this).index();
		 var toggle_arr = [];
		 toggle_arr[i] = false;
		}
		
		$('.m_menu_wrap > ul > li').click(function(){
	         var idx = $(this).index();
			 log(toggle_arr[idx])
			if(!toggle_arr[idx]){
				for(i=0; i<totalnum; i++){
				 toggle_arr[i] = false;
				}
				$('.m_menu_wrap > ul > li').removeClass('click');
				$(this).addClass('click');
				toggle_arr[idx] = true;
			}else{
				$(this).removeClass('click');
				toggle_arr[idx] = false;
			}
		});
	
}



/*------------------------------------------------------------------------------------------------------------------

# 영상 아이프레임 가로 세로 비율 맞추기

--------------------------------------------------------------------------------------------------------------------*/

function movie_raito(){
	
	$(window).resize(function(){
		setRaito();
	});
	
	setRaito()
	function setRaito(){
		var wd = $('.movie').width();
		var hd = (wd/100)*70;
		
		$('.movie').css({"height":hd})
	}
	
}