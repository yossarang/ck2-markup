<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="expires" content="-1" />
<meta name="copyright"content="Cheongkkae">
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>청깨닷컴</title>
<link rel="stylesheet" href="/css/fonts.css" type="text/css" />
<link rel="stylesheet" href="/css/common.css" type="text/css" />
<link rel="stylesheet" href="/css/main.css" type="text/css" />
<link rel="stylesheet" href="/css/intro.css" type="text/css" />
<link rel="stylesheet" href="/css/curriculum.css" type="text/css" />
<link rel="stylesheet" href="/css/dptnews.css" type="text/css" />
<link rel="stylesheet" href="/css/apply.css" type="text/css" />
<link rel="stylesheet" href="/css/notice.css" type="text/css" />
<link rel="stylesheet" href="/css/weekntc.css" type="text/css" />
<link rel="stylesheet" href="/css/jquery.maximage.css" type="text/css" />
<link rel="stylesheet" href="/css/onlyjesus.css" type="text/css" />
<link rel="stylesheet" href="/css/worship.css" type="text/css" />
<link rel="stylesheet" href="/css/letter.css" type="text/css" />


<link rel="stylesheet" href="/css/jquery.mCustomScrollbar.css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!------------------ script -------------------------------->
<script type="text/javascript" src="/js/jquery-latest.js"></script>
<script type='text/javascript' src="/js/jQueryRotate.js"></script> 
<script type='text/javascript' src="/js/jquery_color/jquery.xcolor.js"></script>
<script type='text/javascript' src="/js/jquery_color/jquery.xcolor.min.js"></script>
<script type="text/javascript" src="/js/easing.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<script type="text/javascript" src="/js/nav.js"></script>
<script type="text/javascript" src="/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="/js/jquery.maximage.min.js"></script>
<script type="text/javascript" src="/js/jquery.cycle.all.js"></script>
<script type="text/javascript" src="/js/jquery-ui.js"></script>
</head>

<body>




<!-----------------------------------------------------------------
     #상단영역
------------------------------------------------------------------>
 <section id="head_wrap">
   <div class="head_top_wrap">
   
   <!--------------- 모바일 메뉴 ---------->
   <div class="m_menu_wrap">
      <ul>
       <!-- 청깨소개  -->
        <li>
         <span>청깨소개</span> <span><img src="/img/main/arrow.svg" alt="icon" /></span> 
         
         
           <ul>
             <li><a href="/introduce/intro.php">청깨소개</a></li>
             <li><a href="/introduce/curriculum.php">커리큘럼</a></li>
           </ul>
        </li>
        
        <!-- 국별소식  -->
        <li>
         <span>국별소식</span> <span><img src="/img/main/arrow.svg" alt="icon" /></span>
         
         
           <ul>
             <li><a href="/dpt_news/dpt_news_list.php">국별소식</a></li>
           </ul>
        </li>
        
        <!-- 수강신청  -->
        <li>
         <span>수강신청</span> <span><img src="/img/main/arrow.svg" alt="icon" /></span>
         
         
         <ul>
           <li><a href="/apply/apply_list.php">수강신청</a></li>
         </ul>
        </li>
        
        <!-- 청깨이슈  -->
        <li>
         <span>청깨이슈</span> <span><img src="/img/main/arrow.svg" alt="icon" /></span>
         
         <ul>
           <li><a href="/issue/notice_list.php">공지사항</a></li>
           <li><a href="/issue/weekntc_list.php">이주의 노티스</a></li>
           <li><a href="/issue/yosadclip_list.php">홍보영상</a></li>
         </ul>
        </li>
        
        <!-- 주만ZINE  -->
        <li>
         <span>주만ZINE</span> <span><img src="/img/main/arrow.svg" alt="icon" /></span>
         <ul>
           <li><a href="/onlyjesus/onlyjesus_list.php">주만ZINE</a></li>
         </ul>
        </li>
        
        <!-- 말씀자리  -->
        <li>
         <span>말씀자리</span> <span><img src="/img/main/arrow.svg" alt="icon" /></span>
         
         <ul>
           <li><a href="/word/worship.php">청년예배설교</a></li>
           <li><a href="/word/letter.php">말씀레터</a></li>
         </ul>
         
         
        </li>
      </ul>
   </div>
   
   
   <!--------------- 로고 ------------>
     <a href="/index.php" class="logo"><img src="/img/main/logo.jpg" alt="로고"/></a>
     
   
   <!---- 모바일 스왑 아이콘 --->
  <div class="swapicon_wrap">
     <div class="line"></div>
     <div class="line"></div>
     <div class="line"></div>
     <div class="line"></div>
   </div>  
   
   <!--------------- 공지사항 ------------>
     <div class="notice_wrap ntcshow1">
       <ul>
          <li class="nt_title">NOTICE</li>
          <li>
            <ul class="nt_cont_wrap">
             <li><a href="">청깨닷컴이 새롭게 단장했습니다.청깨닷컴에 도움을 주실 분을 찾습니다.청깨닷컴에 도움을 주실 분을 찾습니다.</a></li>
             <li><a href="">청깨닷컴에 도움을 주실 분을 찾습니다.</a></li>
             <li><a href="">청깨닷컴 임시점검 안내</a></li>
            </ul>
          </li>
          <li><img src="/img/main/notice_arrow_up.png" alt="버튼" class="nt_arrow_up"/></li>
          <li><img src="/img/main/notice_arrow_down.png" alt="버튼" class="nt_arrow_down"/></li>
       </ul>
     </div>
     
     
    
   </div>
   
   <!--------------- 상단 가로선 ------------>
   <div class="line_wrap">
       <ul>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
       </ul>
   </div>
   
  
   
   
<!--------------- 네비게이션 ------------>
   <div class="nav_wrap">
      <ul>
       <!-- 청깨소개  -->
        <li>
         <a href="/introduce/intro.php"> 청깨소개 </a>
         
         
           <ul>
             <li><a href="/introduce/intro.php">청깨소개</a></li>
             <li><a href="/introduce/curriculum.php">커리큘럼</a></li>
           </ul>
        </li>
        
        <!-- 국별소식  -->
        <li>
         <a href="/dpt_news/dpt_news_list.php">국별소식</a>
         
         
           <ul>
             <li><a href="/dpt_news/dpt_news_list.php">국별소식</a></li>
           </ul>
        </li>
        
        <!-- 수강신청  -->
        <li>
         <a href="/apply/apply_list.php">수강신청</a>
         
         
         <ul>
           <li><a href="/apply/apply_list.php">수강신청</a></li>
         </ul>
        </li>
        
        <!-- 청깨이슈  -->
        <li>
         <a href="/issue/notice_list.php">청깨이슈</a>
         
         <ul>
           <li><a href="/issue/notice_list.php">공지사항</a></li>
           <li><a href="/issue/weekntc_list.php">이주의 노티스</a></li>
           <li><a href="/issue/yosadclip_list.php">홍보영상</a></li>
         </ul>
        </li>
        
        <!-- 주만ZINE  -->
        <li>
         <a href="/onlyjesus/onlyjesus_list.php">주만ZINE</a>
         
         
         
         <ul>
           <li><a href="/onlyjesus/onlyjesus_list.php">주만ZINE</a></li>
         </ul>
        </li>
        
        <!-- 말씀자리  -->
        <li>
         <a href="/word/worship.php">말씀자리</a>
         
         <ul>
           <li><a href="/word/worship.php">청년예배설교</a></li>
           <li><a href="/word/letter.php">말씀레터</a></li>
         </ul>
         
         
        </li>
      </ul>
   </div>
  
 </section>