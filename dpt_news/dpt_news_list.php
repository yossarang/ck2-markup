 <?php include('../head.php') ?>
 
<!----------------------------------------------------------------
   # 청깨상단배너
------------------------------------------------------------------>
<section id="dpt_topbanner">
   <div class="topbanner_wrap">
      <div class="top_inner">
        <ul>
          <li>아버지께서 아들을 사랑하사 만물을 다 그의 손에 주셨으니</li>
          <li>요한복음 3:35</li>
        </ul>
       </div> 
   </div>
</section>


<section id="location">
  <div class="location_wrap">
    <ul>
      <li><img src="../img/intro/home_btn.png" alt="homebtn"></li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li>국별소식</li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li class="nowpage">영성국</li>
    </ul>
  </div>
</section>

<section id="dptnews">
  <div class="dptnews_wrap">
  
      <div class="sel_wrap">
         <select name="" class="sel_dpt" onChange="javascript:location.href='<? $_SERVER['php_self'] ?>'">
           <option value="">전체사역국</option>
           <option value="">교육국</option>
           <option value="">영성국</option>
           <option value="">예배국</option>
           <option value="">행사홍보국</option>
           <option value="">사회문화</option>
           <option value="">해외선교</option>
           <option value="">국내선교</option>
           <option value="">북한선교</option>
         </select>
      </div>
  
     <div class="ment">예배를 위한 예배국 입니다.</div>
  
  
      <div class="list_wrap">
      
      <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>예배국</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/dpt_news/dpt_news_read.php">
                   <ul class="cont">
                    <li>신년수련회 콰이어 섬김이 모집</li>
                    <li>신년수련회 섬김을 통해 주님의 임재하심을 경험하고 삶의 기쁨이 회복되는 주일콰이어의 자리에 청년들을 모집합니다.</li>
                   </ul>
               </a>
           </div>
         </div>
         
         
         
         <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>예배국</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/dpt_news/dpt_news_read.php">
                   <ul class="cont">
                    <li>신년수련회 콰이어 섬김이 모집</li>
                    <li>신년수련회 섬김을 통해 주님의 임재하심을 경험하고 삶의 기쁨이 회복되는 주일콰이어의 자리에 청년들을 모집합니다.</li>
                   </ul>
               </a>
           </div>
         </div>
         
         
         
         <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>예배국</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/dpt_news/dpt_news_read.php">
                   <ul class="cont">
                    <li>신년수련회 콰이어 섬김이 모집</li>
                    <li>신년수련회 섬김을 통해 주님의 임재하심을 경험하고 삶의 기쁨이 회복되는 주일콰이어의 자리에 청년들을 모집합니다.</li>
                   </ul>
               </a>
           </div>
         </div>
         
         
         
         <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>예배국</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/dpt_news/dpt_news_read.php">
                   <ul class="cont">
                    <li>신년수련회 콰이어 섬김이 모집</li>
                    <li>신년수련회 섬김을 통해 주님의 임재하심을 경험하고 삶의 기쁨이 회복되는 주일콰이어의 자리에 청년들을 모집합니다.</li>
                   </ul>
               </a>
           </div>
         </div>
         
         
         
         
         <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>예배국</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/dpt_news/dpt_news_read.php">
                   <ul class="cont">
                    <li>신년수련회 콰이어 섬김이 모집</li>
                    <li>신년수련회 섬김을 통해 주님의 임재하심을 경험하고 삶의 기쁨이 회복되는 주일콰이어의 자리에 청년들을 모집합니다.</li>
                   </ul>
               </a>
           </div>
         </div>
         
         
         
         
         <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>예배국</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/dpt_news/dpt_news_read.php">
                   <ul class="cont">
                    <li>신년수련회 콰이어 섬김이 모집</li>
                    <li>신년수련회 섬김을 통해 주님의 임재하심을 경험하고 삶의 기쁨이 회복되는 주일콰이어의 자리에 청년들을 모집합니다.</li>
                   </ul>
               </a>
           </div>
         </div>
         
         
         
         
         <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>예배국</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/dpt_news/dpt_news_read.php">
                   <ul class="cont">
                    <li>신년수련회 콰이어 섬김이 모집</li>
                    <li>신년수련회 섬김을 통해 주님의 임재하심을 경험하고 삶의 기쁨이 회복되는 주일콰이어의 자리에 청년들을 모집합니다.</li>
                   </ul>
               </a>
           </div>
         </div>
         
         
         
         
         <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>예배국</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/dpt_news/dpt_news_read.php">
                   <ul class="cont">
                    <li>신년수련회 콰이어 섬김이 모집</li>
                    <li>신년수련회 섬김을 통해 주님의 임재하심을 경험하고 삶의 기쁨이 회복되는 주일콰이어의 자리에 청년들을 모집합니다.</li>
                   </ul>
               </a>
           </div>
         </div>
         
       
      </div>
  
  
     <div class="more_box">
        SEE MORE
     </div>
  </div>
</section>

<?php include('../bottom.php') ?>
