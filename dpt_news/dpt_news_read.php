 <?php include('../head.php') ?>
 
<!----------------------------------------------------------------
   # 청깨상단배너
------------------------------------------------------------------>
<section id="dpt_topbanner">
   <div class="topbanner_wrap">
      <div class="top_inner">
        <ul>
          <li>아버지께서 아들을 사랑하사 만물을 다 그의 손에 주셨으니</li>
          <li>요한복음 3:35</li>
        </ul>
       </div> 
   </div>
</section>


<section id="location">
  <div class="location_wrap">
    <ul>
      <li><img src="../img/intro/home_btn.png" alt="homebtn"></li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li>국별소식</li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li class="nowpage">영성국</li>
    </ul>
  </div>
</section>

<section id="dptnews">
  <div class="dptnews_wrap">
      <div class="read_wrap">
         
         <div class="dpt_title">예배국</div>
         
         <ul class="title_wrap">
           <li>주일찬양팀 그날의기쁨 팀원모집</li>
           <li>2017.03.27</li>
         </ul>
         
         <hr class="hr2">
         
         <ul class="addfile">
           <li>첨부파일</li>
           <li>|</li>
           <li>
             <ul>
                <li><a href="">예배국_통합사역지원서</a></li>
                <li><a href="">예배국_리더추천서</a></li>
             </ul>
           </li>
         </ul>
         
         <hr class="hr1">
         
         <div class="content_wrap">
         하나님을 사랑하고 예배를 사모하는 마음으로 함께 선 예배자로 하나님을 기쁘게 찬양하길 원하는 팀원을 모집합니다.


■ 모집분야

-보컬 : 인도자, 보컬(남/녀),

-세션 : 건반, 드럼, 기타, 베이스 등

■ 지원내용

-인도자: 20분 분량의 콘티 준비

-보 컬: 내 마음 다해, 자유곡 1곡(악보준비)

-악 기: 내 마음 다해, 자유곡 1곡(악보준비)

■ 지원자격

– 사랑국 한 텀 이상

– 사역팀 1년 이상 섬길 수 있는 분

■ 모집시기

-3월 31일 (금)까지 (면접 및 오디션 일정은 추후공지)

■ 신청문의:

주일찬양팀악기팀장(010-6346-0701,joyworship0306@gmail.com)

※ 지원서는 지원자가 작성 후 메일로 접수
 리더추천서는 작성 후 리더가 직접 접수메일로 발송


         </div>
         
         <div class="prev_wrap">
           <ul>
             <a href="">
             <li>주일찬양팀 그날의 기쁨 팀원모집</li>
             <li>PREV <img src="../img/dptnews/prev_arr.png" alt="아이콘"/></li>
             </a>
           </ul>
         </div>
         
         <div class="next_wrap">
           <ul>
             <a href="">
             <li>주일찬양팀 그날의 기쁨 팀원모집</li>
             <li>NEXT <img src="../img/dptnews/prev_arr.png" alt="아이콘"/></li>
             </a>
           </ul>
         </div>
         
         <div class="share_wrap">
             <ul class="share">
               <li><a href=""></a></li>
               <li><a href=""></a></li>
               <li><a href=""></a></li>
               <li><a href=""></a></li>
             </ul>
         </div>
         
         <a href="dpt_news_list.php" class="bnt" >LIST</a>
         
      </div>
  </div>
</section>

<?php include('../bottom.php') ?>
