 <?php include('../head.php') ?>
 
<!----------------------------------------------------------------
   # 청깨상단배너
------------------------------------------------------------------>
<section id="intro_topbanner">
   <div class="topbanner_wrap">
      <div class="top_inner">
        <img src="../img/intro/top_img1.png" />
        <ul>
          <li>하나님이 세상을 이처럼 사랑하사 독생자를 주셨의 이는 그를 밎는 자마다 멸망하지 않고 영생을 얻게 하려 하심이라</li>
          <li>요한복음 3:16</li>
        </ul>
       </div> 
   </div>
</section>


<section id="location">
  <div class="location_wrap">
    <ul>
      <li><img src="../img/intro/home_btn.png" alt="homebtn"></li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li>청깨소개</li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li class="nowpage">청깨소개</li>
    </ul>
  </div>
</section>

<section id="philos1">
  <div class="philos_wrap">
    <ul class="title">
      <li class="tick"></li>
      <li>PHILOSOPHY 1.</li>
      <li>하나님나라의 청년 제자운동</li>
    </ul> 
    
    <ul class="cont1">
      <li>사랑의교회 청년부는 세상 속에 임한 하나님나라를 주도적이고 창조적으로 구현하기 위해 ‘하나님나라의 청년제자 운동’을 비전으로 삼고 있습니다.</li>
    </ul>
    
    
        <ul class="card card1">
          <li></li>
          <li>
            <ul>
              <li>복음주의 영성</li>
              <li>말씀과 기도로함께하는 복음주의 영성</li>
            </ul>
          </li>
        </ul>
        
        <ul class="card card2">
          <li></li>
          <li>
            <ul>
              <li>일터에서의 소명자</li>
              <li>사회 속에서 더불어 실천하는 소명자</li>
            </ul>
          </li>
        </ul>
        
        <ul class="card card3">
          <li></li>
          <li>
            <ul>
              <li>세상을 품은 그리스도인</li>
              <li>벽을 넘어 세상을 품은 그리스도인</li>
            </ul>
          </li>
        </ul>

  </div>
</section>


<section id="philos2">
  <div class="philos_wrap">
   <ul class="title">
      <li class="tick"></li>
      <li>Philosophy 2.</li>
      <li>청년을 깨운다</li>
   </ul> 
   
  <ul class="cont1">
      <li>“청년을 깨운다”의 줄임말인 “청깨 아카데미”는 복음주의 영성을 가지고 일터에서 세상을 품는 청년제자를 양육하기 위한 교육 커리큘럼입니다.</li>
   </ul>
   
  <img src="../img/intro/pic1.png"  class="issue_pic1">
  </div>
</section>

<?php include('../bottom.php') ?>
