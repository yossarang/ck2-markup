 <?php include('../head.php') ?>
 
<!----------------------------------------------------------------
   # 청깨상단배너
------------------------------------------------------------------>
<section id="intro_topbanner">
   <div class="topbanner_wrap">
      <div class="top_inner">
        <img src="../img/intro/top_img1.png" />
        <ul>
          <li>하나님이 세상을 이처럼 사랑하사 독생자를 주셨의 이는 그를 밎는 자마다 멸망하지 않고 영생을 얻게 하려 하심이라</li>
          <li>요한복음 3:16</li>
        </ul>
       </div> 
   </div>
</section>


<section id="location">
  <div class="location_wrap">
    <ul>
      <li><img src="../img/intro/home_btn.png" alt="homebtn"></li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li>청깨소개</li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li class="nowpage">커리큘럼</li>
    </ul>
  </div>
</section>

<section id="curriculum">
  <div class="curri_wrap">
     
     <ul class="title">
       <li>CURRICULUM</li>
       <li>청깨아카데미 커리큘럼</li>
     </ul>
     
     <P class="table_title">2017 상반기 청깨 아카데미</P>
     
     <div class="table_wrap">
       <ul class="cur_title">
         <li>영성국/큐티학교</li>
         <li>영성국/기도학교</li>
         <li>사일국/일터소명학교</li>
         <li>사일국/기독교세계관학교</li>
       </ul>
       
     
      <div class="cur_table">
      
      <ul class="month1 month">
              <li>4</li>
              <li class="line"><div></div></li>
      </ul>
           
      <ul class="month2 month">
              <li>5</li>
              <li class="line"><div></div></li>
      </ul>
           
      <ul class="month3 month">
              <li>6</li>
              <li class="line"><div></div></li>
       </ul>
       
       
           <ul class="table_set">
             <li><div class="cul"><p>3/25~4/25(4주)</p></div></li>
             <li><div class="cul"><p>3/25~4/25(4주)</p></div></li>
             <li><div class="cul"><p>4/8~5/6(8주)</p></div></li>
             <li><div class="cul"><p>5/20~6/17(4주)</p></div></li>
           </ul>
       
       </div>
     </div>
  </div>
</section>

<?php include('../bottom.php') ?>
