 <?php include('../head.php') ?>
 
<!----------------------------------------------------------------
   # 청깨상단배너
------------------------------------------------------------------>
<section id="issue_topbanner">
   <div class="topbanner_wrap">
      <div class="top_inner">
        <img src="../img/apply/top_img.png" alt="이미지" />
        <ul>
          <li>내가 그리스도와 함께 십자가에 못 박혔나니 그런즉 이제는 내가 사는 것이 아니요 오직 내 안에 그리스도께서 사시는 것이라</li>
          <li>갈라디아서 2:20</li>
        </ul>
       </div> 
   </div>
</section>


<section id="location">
  <div class="location_wrap">
    <ul>
      <li><a href="/index.php"><img src="../img/intro/home_btn.png" alt="homebtn"></a></li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li>청깨이슈</li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li class="nowpage">이주의 노티스</li>
    </ul>
  </div>
</section>

<section id="weekntc">
  <div class="weekntc_wrap">
  
     <ul class="title">
       <li>이주의 노티스</li>
       <li>THIS WEEK NOTICE MOVIE</li>
     </ul>
  
      <div class="read_wrap">
        
       <ul class="title_wrap">
         <li>THIS WEEK YOS NOTICE</li>
         <li>2017.03.24</li>
       </ul>
       
       <hr class="hr1" />
       
       <iframe src="https://www.youtube.com/embed/SiAuF91kfJ8" frameborder="0" allowfullscreen class="movie"></iframe>
      
       
       <div class="share_wrap">
             <ul class="share">
               <li><a href=""></a></li>
               <li><a href=""></a></li>
               <li><a href=""></a></li>
               <li><a href=""></a></li>
             </ul>
         </div>
      
       <a href="/issue/weekntc_list.php" class="bnt center">LIST</a>

     
     
     </div>
  </div>
</section>

<?php include('../bottom.php') ?>
