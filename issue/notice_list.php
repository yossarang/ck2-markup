 <?php include('../head.php') ?>
 
<!----------------------------------------------------------------
   # 청깨상단배너
------------------------------------------------------------------>
<section id="issue_topbanner">
   <div class="topbanner_wrap">
      <div class="top_inner">
        <img src="../img/apply/top_img.png" alt="이미지" />
        <ul>
          <li>내가 그리스도와 함께 십자가에 못 박혔나니 그런즉 이제는 내가 사는 것이 아니요 오직 내 안에 그리스도께서 사시는 것이라</li>
          <li>갈라디아서 2:20</li>
        </ul>
       </div> 
   </div>
</section>


<section id="location">
  <div class="location_wrap">
    <ul>
      <li><a href="/index.php"><img src="../img/intro/home_btn.png" alt="homebtn"></a></li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li>청깨이슈</li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li class="nowpage">공지사항</li>
    </ul>
  </div>
</section>

<section id="ntc">
  <div class="ntc_wrap">
  
     <ul class="title">
       <li>공지사항</li>
       <li>NOTICE & NEWS</li>
     </ul>
  
      <div class="list_wrap">
        
        <ul class="title_wrap">
          <li>번호</li>
          <li>제목</li>
          <li>날짜</li>
        </ul>
        
        <hr class="hr2" />
        
        <ul class="list">
        <!-- 게시글 리스트 -->
          <li>
            <ul>
                <li>1</li>
                <li><a href="/issue/notice_read.php">청깨에서 인재를 찾습니다.</a></li>
                <li>2017.03.24</li>
            </ul>
          </li>
          
          <!-- 게시글 리스트 -->
          <li>
            <ul>
                <li>1</li>
                <li><a href="/issue/notice_read.php">청깨에서 인재를 찾습니다.</a></li>
                <li>2017.03.24</li>
            </ul>
          </li>
          
          <!-- 게시글 리스트 -->
          <li>
            <ul>
                <li>1</li>
                <li><a href="/issue/notice_read.php">청깨에서 인재를 찾습니다.</a></li>
                <li>2017.03.24</li>
            </ul>
          </li>
          
          <!-- 게시글 리스트 -->
          <li>
            <ul>
                <li>1</li>
                <li><a href="/issue/notice_read.php">청깨에서 인재를 찾습니다.</a></li>
                <li>2017.03.24</li>
            </ul>
          </li>
          
          <!-- 게시글 리스트 -->
          <li>
            <ul>
                <li>1</li>
                <li><a href="/issue/notice_read.php">청깨에서 인재를 찾습니다.</a></li>
                <li>2017.03.24</li>
            </ul>
          </li>
          
          <!-- 게시글 리스트 -->
          <li>
            <ul>
                <li>1</li>
                <li><a href="/issue/notice_read.php">청깨에서 인재를 찾습니다.</a></li>
                <li>2017.03.24</li>
            </ul>
          </li>
          
          <!-- 게시글 리스트 -->
          <li>
            <ul>
                <li>1</li>
                <li><a href="/issue/notice_read.php">청깨에서 인재를 찾습니다.</a></li>
                <li>2017.03.24</li>
            </ul>
          </li>
          
          <!-- 게시글 리스트 -->
          <li>
            <ul>
                <li>1</li>
                <li><a href="/issue/notice_read.php">청깨에서 인재를 찾습니다.</a></li>
                <li>2017.03.24</li>
            </ul>
          </li>
          
          <!-- 게시글 리스트 -->
          <li>
            <ul>
                <li>1</li>
                <li><a href="/issue/notice_read.php">청깨에서 인재를 찾습니다.</a></li>
                <li>2017.03.24</li>
            </ul>
          </li>
          
          <!-- 게시글 리스트 -->
          <li>
            <ul>
                <li>1</li>
                <li><a href="/issue/notice_read.php">청깨에서 인재를 찾습니다.</a></li>
                <li>2017.03.24</li>
            </ul>
          </li>
        </ul>
         
        
        <div class="paging_wrap">
        
          <ul>
            <li><a href=""><img src="../img/issue/paging_left_arrow1.png" /></a></li>
            <li><a href=""><img src="../img/issue/paging_left_arrow2.png" /></a></li>
            
            <li class="nowpage">1</li>
            <li><a href="" class="paging_bnt">2</a></li>
            <li><a href="" class="paging_bnt">3</a></li>
            <li><a href="" class="paging_bnt">4</a></li>
            <li><a href="" class="paging_bnt">5</a></li>
            
            <li><a href=""><img src="../img/issue/paging_right_arrow1.png" /></a></li>
            <li><a href=""><img src="../img/issue/paging_right_arrow2.png" /></a></li>
          </ul>
        </div> 
         
         
      </div>
  </div>
</section>

<?php include('../bottom.php') ?>
