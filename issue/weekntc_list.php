 <?php include('../head.php') ?>
 
<!----------------------------------------------------------------
   # 청깨상단배너
------------------------------------------------------------------>
<section id="issue_topbanner">
   <div class="topbanner_wrap">
      <div class="top_inner">
        <img src="../img/apply/top_img.png" alt="이미지" />
        <ul>
          <li>내가 그리스도와 함께 십자가에 못 박혔나니 그런즉 이제는 내가 사는 것이 아니요 오직 내 안에 그리스도께서 사시는 것이라</li>
          <li>갈라디아서 2:20</li>
        </ul>
       </div> 
   </div>
</section>


<section id="location">
  <div class="location_wrap">
    <ul>
      <li><a href="/index.php"><img src="../img/intro/home_btn.png" alt="homebtn"></a></li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li>청깨이슈</li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li class="nowpage">이주의 노티스</li>
    </ul>
  </div>
</section>

<section id="weekntc">
  <div class="weekntc_wrap">
  
     <ul class="title">
       <li>이주의 노티스</li>
       <li>THIS WEEK NOTICE MOVIE</li>
     </ul>
  
      <div class="list_wrap">
        
        <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>YOS NOTICE</li>
                <li>2017.03.27</li>
               </ul>
           </div>
          
          <a href="/issue/weekntc_read.php"> 
          <div class="holder" >   
            <div class="maximage">
             <img src="../img/issue/sample_img1.jpg" />
            </div>
          </div>
          </a>
              
         </div>
         
         
         
         <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>YOS NOTICE</li>
                <li>2017.03.27</li>
               </ul>
           </div>
          
          <a href="/issue/weekntc_read.php"> 
          <div class="holder" >   
            <div class="maximage">
             <img src="../img/issue/sample_img2.jpg" />
            </div>
          </div>
          </a>
              
         </div>
         
         
            <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>YOS NOTICE</li>
                <li>2017.03.27</li>
               </ul>
           </div>
          
          <a href="/issue/weekntc_read.php"> 
          <div class="holder" >   
            <div class="maximage">
             <img src="../img/issue/sample_img3.jpg" />
            </div>
          </div>
          </a>
              
         </div>
         
         
         <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>YOS NOTICE</li>
                <li>2017.03.27</li>
               </ul>
           </div>
          
          <a href="/issue/weekntc_read.php"> 
          <div class="holder" >   
            <div class="maximage">
             <img src="../img/issue/sample_img1.jpg" />
            </div>
          </div>
          </a>
              
         </div>
         
         
         
          <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>YOS NOTICE</li>
                <li>2017.03.27</li>
               </ul>
           </div>
          
          <a href="/issue/weekntc_read.php"> 
          <div class="holder" >   
            <div class="maximage">
             <img src="../img/issue/sample_img1.jpg" />
            </div>
          </div>
          </a>
              
         </div>
         
         
         
         <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>YOS NOTICE</li>
                <li>2017.03.27</li>
               </ul>
           </div>
          
          <a href="/issue/weekntc_read.php"> 
          <div class="holder" >   
            <div class="maximage">
             <img src="../img/issue/sample_img2.jpg" />
            </div>
          </div>
          </a>
              
         </div>
         
         
            <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>YOS NOTICE</li>
                <li>2017.03.27</li>
               </ul>
           </div>
          
          <a href="/issue/weekntc_read.php"> 
          <div class="holder">   
            <div class="maximage">
             <img src="../img/issue/sample_img3.jpg" />
            </div>
          </div>
          </a>
              
         </div>
         
         
         <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>YOS NOTICE</li>
                <li>2017.03.27</li>
               </ul>
           </div>
          
          <a href="/issue/weekntc_read.php"> 
          <div class="holder" >   
            <div class="maximage">
             <img src="../img/issue/sample_img1.jpg" />
            </div>
          </div>
          </a>
              
         </div>
         
         
      </div>
      
      
      
      <div class="more_box">
        SEE MORE
     </div>
      
  </div>
</section>

<?php include('../bottom.php') ?>
