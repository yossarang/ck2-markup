 <?php include('../head.php') ?>
 
<!----------------------------------------------------------------
   # 청깨상단배너
------------------------------------------------------------------>
<section id="word_topbanner">
   <div class="topbanner_wrap">
      <img src="../img/word/top_img1.png" alt="이미지" class="timg1"/>
      <img src="../img/word/top_img2.png" alt="이미지" class="timg2"/>
      <div class="top_inner">
        <ul>
          <li>그가 나를 푸른 풀밭에 누이시며 쉴 만한 물 가로 인도하시는 도다</li>
          <li>시편 23:2</li>
        </ul>
       </div> 
   </div>
</section>


<section id="location">
  <div class="location_wrap">
    <ul>
      <li><a href="/index.php"><img src="../img/intro/home_btn.png" alt="homebtn"></a></li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li>말씀자리</li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li class="nowpage">청년예배설교</li>
    </ul>
  </div>
</section>

<section id="worship">
  <div class="worship_wrap">
  
     <ul class="title">
       <li>청년예배설교</li>
       <li>YOUTH WORSHIP</li>
     </ul>
  
      <div class="list_wrap">
        
        <ul class="date_wrap">
           <li><a href=""><img src="../img/word/arrow_left.png" /></a></li>
           <li>2017.03.24</li>
           <li><a href=""><img src="../img/word/arrow_right.png" /></a></li>
        </ul>
        
        <ul class="title_wrap">
          <li>"제자도 슈퍼 그리스도인들만의 것인가?"</li>
          <li>베드로후서 3:18  김승진 목사님</li>
        </ul>   
        
        
        <iframe src="https://www.youtube.com/embed/SiAuF91kfJ8" frameborder="0" allowfullscreen class="movie"></iframe>
        
         
      </div>
     
      
  </div>
</section>

<?php include('../bottom.php') ?>
