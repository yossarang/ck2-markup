 <?php include('../head.php') ?>
 
<!----------------------------------------------------------------
   # 청깨상단배너
------------------------------------------------------------------>
<section id="word_topbanner">
   <div class="topbanner_wrap">
      <img src="../img/word/top_img1.png" alt="이미지" class="timg1"/>
      <img src="../img/word/top_img2.png" alt="이미지" class="timg2"/>
      <div class="top_inner">
        <ul>
          <li>그가 나를 푸른 풀밭에 누이시며 쉴 만한 물 가로 인도하시는 도다</li>
          <li>시편 23:2</li>
        </ul>
       </div> 
   </div>
</section>


<section id="location">
  <div class="location_wrap">
    <ul>
      <li><a href="/index.php"><img src="../img/intro/home_btn.png" alt="homebtn"></a></li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li>말씀자리</li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li class="nowpage">말씀레터</li>
    </ul>
  </div>
</section>

<section id="letter">
  <div class="letter_wrap">
  
     <ul class="title">
       <li>말씀레터</li>
       <li>BIBLE LETTER</li>
     </ul>
  
      <div class="list_wrap">
        
        <ul class="date_wrap">
           <li><a href=""><img src="../img/word/arrow_left2.png" class="arr"/></a></li>
           <li><img src="../img/word/calendar.png" id="datepicker" class="cal"/> 2017.03.24</li>
           <li><a href=""><img src="../img/word/arrow_right2.png" class="arr"/></a></li>
        </ul>
        
        
       <div class="box_wrap"> 
           <div class="box">
             <div class="holder">
               <div class="maximage">
                 <img src="../img/word/img_sample.jpg" />
               </div>
             </div>
          </div>
      
          <div class="letter_cont">
              <ul>
               <li>이는 나 여호와 하나님이 네 오른손을 붙들고 네게 이르기를 두려워하지 말라 내가 너를 도우리라할 것임이라</li>
               <li>이사야 41:13</li>
              </ul>
          </div>
      </div>
      
      
      
                
      </div>
     
      
  </div>
</section>

<?php include('../bottom.php') ?>
