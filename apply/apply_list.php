 <?php include('../head.php') ?>
 
<!----------------------------------------------------------------
   # 청깨상단배너
------------------------------------------------------------------>
<section id="apply_topbanner">
   <div class="topbanner_wrap">
      <div class="top_inner">
        <img src="../img/apply/top_img.png" alt="이미지" />
        <ul>
          <li>내가 그리스도와 함께 십자가에 못 박혔나니 그런즉 이제는 내가 사는 것이 아니요 오직 내 안에 그리스도께서 사시는 것이라</li>
          <li>갈라디아서 2:20</li>
        </ul>
       </div> 
   </div>
</section>


<section id="location">
  <div class="location_wrap">
    <ul>
      <li><a href="/index.php"><img src="../img/intro/home_btn.png" alt="homebtn"></a></li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li>수강신청</li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li class="nowpage">수강신청</li>
    </ul>
  </div>
</section>

<section id="apply">
  <div class="apply_wrap">
  
     <ul class="title">
       <li>수강신청</li>
       <li>APPLYCATION FOR CLASSES</li>
     </ul>
  
  
    <!-- <ul class="school_list">
       <li class="on">큐티학교</li>
       <li class="did">|</li>
       <li><a href="">일터선교세미나</a></li>
       <li class="did">|</li>
       <li><a href="">소명학교</a></li>
       <li class="did">|</li>
       <li><a href="">전도학교</a></li>
       <li class="did">|</li>
       <li><a href="">기독교세계관</a></li>
       <li class="did">|</li>
       <li><a href="">수련회</a></li>
       <li class="did">|</li>
       <li><a href="">성경맥잡기</a></li>
       <li class="did">|</li>
     </ul> -->
  
      <div class="list_wrap">
      
      <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>큐티학교</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/apply/apply_read.php">
                   <ul class="cont">
                    <li>접수기간 : 2017.03.04 ~ 05.29</li>
                    <li>매일매일 하나님과 만나는 시간인 Quiet Time을 어떻게 보내야 하는지 배우고, 함께 나누며 은혜를 누리는 큐티학교로 초대합니다. 제자훈련 신청 필수 과정인 큐티학교에 많은 참여 기다립니다.</li>
                   </ul>
               </a>
           </div>
           <a href="" class="apply_bnt on">수강신청 바로가기</a>
         </div>
         
         
          <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>일터선교세미나</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/apply/apply_read.php">
                   <ul class="cont">
                    <li>접수기간 : 2017.03.04 ~ 05.29</li>
                    <li>매일매일 하나님과 만나는 시간인 Quiet Time을 어떻게 보내야 하는지 배우고, 함께 나누며 은혜를 누리는 큐티학교로 초대합니다. 제자훈련 신청 필수 과정인 큐티학교에 많은 참여 기다립니다.</li>
                   </ul>
               </a>
           </div>
           <a href="" class="apply_bnt on">수강신청 바로가기</a>
         </div>
         
         
          <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>소명학교</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/apply/apply_read.php">
                   <ul class="cont">
                    <li>접수기간 : 2017.03.04 ~ 05.29</li>
                    <li>매일매일 하나님과 만나는 시간인 Quiet Time을 어떻게 보내야 하는지 배우고, 함께 나누며 은혜를 누리는 큐티학교로 초대합니다. 제자훈련 신청 필수 과정인 큐티학교에 많은 참여 기다립니다.</li>
                   </ul>
               </a>
           </div>
           <a href="" class="apply_bnt on">수강신청 바로가기</a>
         </div>
         
         
          <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>전도학교</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/apply/apply_read.php">
                   <ul class="cont">
                    <li>접수기간 : 2017.03.04 ~ 05.29</li>
                    <li>매일매일 하나님과 만나는 시간인 Quiet Time을 어떻게 보내야 하는지 배우고, 함께 나누며 은혜를 누리는 큐티학교로 초대합니다. 제자훈련 신청 필수 과정인 큐티학교에 많은 참여 기다립니다.</li>
                   </ul>
               </a>
           </div>
           <a href="" class="apply_bnt">수강신청 기간이 아닙니다</a>
         </div>
         
         
          <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>기독교세계관</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/apply/apply_read.php">
                   <ul class="cont">
                    <li>접수기간 : 2017.03.04 ~ 05.29</li>
                    <li>매일매일 하나님과 만나는 시간인 Quiet Time을 어떻게 보내야 하는지 배우고, 함께 나누며 은혜를 누리는 큐티학교로 초대합니다. 제자훈련 신청 필수 과정인 큐티학교에 많은 참여 기다립니다.</li>
                   </ul>
               </a>
           </div>
           <a href="" class="apply_bnt on">수강신청 바로가기</a>
         </div>
         
         
          <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>수련회</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/apply/apply_read.php">
                   <ul class="cont">
                    <li>접수기간 : 2017.03.04 ~ 05.29</li>
                    <li>매일매일 하나님과 만나는 시간인 Quiet Time을 어떻게 보내야 하는지 배우고, 함께 나누며 은혜를 누리는 큐티학교로 초대합니다. 제자훈련 신청 필수 과정인 큐티학교에 많은 참여 기다립니다.</li>
                   </ul>
               </a>
           </div>
           <a href="" class="apply_bnt">수강신청 기간이 아닙니다.</a>
         </div>
         
         
          <!--------------------- box ------------------>
         <div class="box">
           <div class="content">
              <ul class="title">
                <li>성격맥잡기</li>
                <li>2017.03.27</li>
               </ul>
               <div class="line"></div>
               <a href="/apply/apply_read.php">
                   <ul class="cont">
                    <li>접수기간 : 2017.03.04 ~ 05.29</li>
                    <li>매일매일 하나님과 만나는 시간인 Quiet Time을 어떻게 보내야 하는지 배우고, 함께 나누며 은혜를 누리는 큐티학교로 초대합니다. 제자훈련 신청 필수 과정인 큐티학교에 많은 참여 기다립니다.</li>
                   </ul>
               </a>
           </div>
           <a href="" class="apply_bnt">수강신청 기간이 아닙니다.</a>
         </div>
         

         
         
        
         
       
      </div>
  
  
     <div class="more_box">
        SEE MORE
     </div>
  </div>
</section>

<?php include('../bottom.php') ?>
