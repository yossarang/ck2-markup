 <?php include('../head.php') ?>
 
<!----------------------------------------------------------------
   # 청깨상단배너
------------------------------------------------------------------>
<section id="onlyjesus_topbanner">
   <div class="topbanner_wrap">
      <img src="../img/onlyjesus/top_img1.png" alt="이미지" class="timg1"/>
      <img src="../img/onlyjesus/top_img2.png" alt="이미지" class="timg2"/>
      <div class="top_inner">
        <ul>
          <li>사랑은 여기 있으니 우리가 하나님을 사랑한 것이 아니요 하나님이 우리를 사랑하사 우리 죄를 속하기 위하여 화목제물로 그 아들을 보내셨음이라</li>
          <li>요한복음 1:15</li>
        </ul>
       </div> 
   </div>
</section>


<section id="location">
  <div class="location_wrap">
    <ul>
      <li><a href="/index.php"><img src="../img/intro/home_btn.png" alt="homebtn"></a></li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li>주만ZINE</li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li class="nowpage">주만ZINE</li>
    </ul>
  </div>
</section>

<section id="onlyjesus">
  <div class="onlyjesus_wrap">
  
     <ul class="title">
       <li>주만ZINE</li>
       <li>ONLY JESUS</li>
     </ul>
  
      <div class="list_wrap">
        
        <!--------------------- box ------------------>
         <div class="box">
     
          <a href="/onlyjesus/onlyjesus_read.php"> 
          <div class="jesusholder" >   
            <div class="maximage">
             <img src="../img/onlyjesus/sample_img.jpg" />
            </div>
          </div>
          </a>
          
          <div class="content">
              <ul class="title">
                <li>국장단 인터뷰</li>
                <li>2017.03.27</li>
              </ul>
              
              <div class="cont">
              <a href="/onlyjesus/onlyjesus_read.php">
                “비가와도 너무 보람차고 즐겁고 좋은 시간이었던 것 같습니다.” 손과 팔이 굽이 제대로 사용하지 못하시는 할머니와 할아버지들 의 모습을 보면서 마음이 많이 아팠습니다. 하지만 그 분들이
저희들 보다 훨씬 굳세고 밝은 모습으로 살고 계시다는 걸 금새 발견 할 수 있었죠. 이 분들은 나이가 80, 90이 넘으셔도 꿈을 간직하고 계셨습니다. 그저 꿈만 꾸시는게 아니라 계획이 있고 그 계확아래 실천을 해가며 소망을 가꾸고 계셨습니다. 과연 우리 청년들은 언제부터 현실과 세상이란 벽에 막혀 꿈을 잃고 소망을 잃고 살았는지 모르겠습니다. 누구보다 어렵고 척박한 환경에서도 꿈을 이어가는 어르신들의 모습에 잃어 버렸던 열정과 꿈에대한 비젼을 되찾는 시간이 되었습니다.
</a>
              </div>
           </div>
              
         </div>
         
         
         
          <!--------------------- box ------------------>
         <div class="box">
     
          <a href="/onlyjesus/onlyjesus_read.php"> 
          <div class="jesusholder" >   
            <div class="maximage">
             <img src="../img/onlyjesus/sample_img.jpg" />
            </div>
          </div>
          </a>
          
          <div class="content">
              <ul class="title">
                <li>국장단 인터뷰</li>
                <li>2017.03.27</li>
              </ul>
              
              <div class="cont">
              <a href="/onlyjesus/onlyjesus_read.php">
                “비가와도 너무 보람차고 즐겁고 좋은 시간이었던 것 같습니다.” 손과 팔이 굽이 제대로 사용하지 못하시는 할머니와 할아버지들 의 모습을 보면서 마음이 많이 아팠습니다. 하지만 그 분들이
저희들 보다 훨씬 굳세고 밝은 모습으로 살고 계시다는 걸 금새 발견 할 수 있었죠. 이 분들은 나이가 80, 90이 넘으셔도 꿈을 간직하고 계셨습니다. 그저 꿈만 꾸시는게 아니라 계획이 있고 그 계확아래 실천을 해가며 소망을 가꾸고 계셨습니다. 과연 우리 청년들은 언제부터 현실과 세상이란 벽에 막혀 꿈을 잃고 소망을 잃고 살았는지 모르겠습니다. 누구보다 어렵고 척박한 환경에서도 꿈을 이어가는 어르신들의 모습에 잃어 버렸던 열정과 꿈에대한 비젼을 되찾는 시간이 되었습니다.
</a>
              </div>
           </div>
              
         </div>
         
         
         
         
           <!--------------------- box ------------------>
         <div class="box">
     
          <a href="/onlyjesus/onlyjesus_read.php"> 
          <div class="jesusholder" >   
            <div class="maximage">
             <img src="../img/onlyjesus/sample_img.jpg" />
            </div>
          </div>
          </a>
          
          <div class="content">
              <ul class="title">
                <li>국장단 인터뷰</li>
                <li>2017.03.27</li>
              </ul>
              
              <div class="cont">
              <a href="/onlyjesus/onlyjesus_read.php">
                “비가와도 너무 보람차고 즐겁고 좋은 시간이었던 것 같습니다.” 손과 팔이 굽이 제대로 사용하지 못하시는 할머니와 할아버지들 의 모습을 보면서 마음이 많이 아팠습니다. 하지만 그 분들이
저희들 보다 훨씬 굳세고 밝은 모습으로 살고 계시다는 걸 금새 발견 할 수 있었죠. 이 분들은 나이가 80, 90이 넘으셔도 꿈을 간직하고 계셨습니다. 그저 꿈만 꾸시는게 아니라 계획이 있고 그 계확아래 실천을 해가며 소망을 가꾸고 계셨습니다. 과연 우리 청년들은 언제부터 현실과 세상이란 벽에 막혀 꿈을 잃고 소망을 잃고 살았는지 모르겠습니다. 누구보다 어렵고 척박한 환경에서도 꿈을 이어가는 어르신들의 모습에 잃어 버렸던 열정과 꿈에대한 비젼을 되찾는 시간이 되었습니다.
</a>
              </div>
           </div>
              
         </div>
         
         
         
         
           <!--------------------- box ------------------>
         <div class="box">
     
          <a href="/onlyjesus/onlyjesus_read.php"> 
          <div class="jesusholder" >   
            <div class="maximage">
             <img src="../img/onlyjesus/sample_img.jpg" />
            </div>
          </div>
          </a>
          
          <div class="content">
              <ul class="title">
                <li>국장단 인터뷰</li>
                <li>2017.03.27</li>
              </ul>
              
              <div class="cont">
              <a href="/onlyjesus/onlyjesus_read.php">
                “비가와도 너무 보람차고 즐겁고 좋은 시간이었던 것 같습니다.” 손과 팔이 굽이 제대로 사용하지 못하시는 할머니와 할아버지들 의 모습을 보면서 마음이 많이 아팠습니다. 하지만 그 분들이
저희들 보다 훨씬 굳세고 밝은 모습으로 살고 계시다는 걸 금새 발견 할 수 있었죠. 이 분들은 나이가 80, 90이 넘으셔도 꿈을 간직하고 계셨습니다. 그저 꿈만 꾸시는게 아니라 계획이 있고 그 계확아래 실천을 해가며 소망을 가꾸고 계셨습니다. 과연 우리 청년들은 언제부터 현실과 세상이란 벽에 막혀 꿈을 잃고 소망을 잃고 살았는지 모르겠습니다. 누구보다 어렵고 척박한 환경에서도 꿈을 이어가는 어르신들의 모습에 잃어 버렸던 열정과 꿈에대한 비젼을 되찾는 시간이 되었습니다.
</a>
              </div>
           </div>
              
         </div>
         
         
         
         
           <!--------------------- box ------------------>
         <div class="box">
     
          <a href="/onlyjesus/onlyjesus_read.php"> 
          <div class="jesusholder" >   
            <div class="maximage">
             <img src="../img/onlyjesus/sample_img.jpg" />
            </div>
          </div>
          </a>
          
          <div class="content">
              <ul class="title">
                <li>국장단 인터뷰</li>
                <li>2017.03.27</li>
              </ul>
              
              <div class="cont">
              <a href="/onlyjesus/onlyjesus_read.php">
                “비가와도 너무 보람차고 즐겁고 좋은 시간이었던 것 같습니다.” 손과 팔이 굽이 제대로 사용하지 못하시는 할머니와 할아버지들 의 모습을 보면서 마음이 많이 아팠습니다. 하지만 그 분들이
저희들 보다 훨씬 굳세고 밝은 모습으로 살고 계시다는 걸 금새 발견 할 수 있었죠. 이 분들은 나이가 80, 90이 넘으셔도 꿈을 간직하고 계셨습니다. 그저 꿈만 꾸시는게 아니라 계획이 있고 그 계확아래 실천을 해가며 소망을 가꾸고 계셨습니다. 과연 우리 청년들은 언제부터 현실과 세상이란 벽에 막혀 꿈을 잃고 소망을 잃고 살았는지 모르겠습니다. 누구보다 어렵고 척박한 환경에서도 꿈을 이어가는 어르신들의 모습에 잃어 버렸던 열정과 꿈에대한 비젼을 되찾는 시간이 되었습니다.
</a>
              </div>
           </div>
              
         </div>
         
         
         
         
           <!--------------------- box ------------------>
         <div class="box">
     
          <a href="/onlyjesus/onlyjesus_read.php"> 
          <div class="jesusholder" >   
            <div class="maximage">
             <img src="../img/onlyjesus/sample_img.jpg" />
            </div>
          </div>
          </a>
          
          <div class="content">
              <ul class="title">
                <li>국장단 인터뷰</li>
                <li>2017.03.27</li>
              </ul>
              
              <div class="cont">
              <a href="/onlyjesus/onlyjesus_read.php">
                “비가와도 너무 보람차고 즐겁고 좋은 시간이었던 것 같습니다.” 손과 팔이 굽이 제대로 사용하지 못하시는 할머니와 할아버지들 의 모습을 보면서 마음이 많이 아팠습니다. 하지만 그 분들이
저희들 보다 훨씬 굳세고 밝은 모습으로 살고 계시다는 걸 금새 발견 할 수 있었죠. 이 분들은 나이가 80, 90이 넘으셔도 꿈을 간직하고 계셨습니다. 그저 꿈만 꾸시는게 아니라 계획이 있고 그 계확아래 실천을 해가며 소망을 가꾸고 계셨습니다. 과연 우리 청년들은 언제부터 현실과 세상이란 벽에 막혀 꿈을 잃고 소망을 잃고 살았는지 모르겠습니다. 누구보다 어렵고 척박한 환경에서도 꿈을 이어가는 어르신들의 모습에 잃어 버렸던 열정과 꿈에대한 비젼을 되찾는 시간이 되었습니다.
</a>
              </div>
           </div>
              
         </div>
        
         
      </div>
      
      
      
      <div class="more_box">
        SEE MORE
     </div>
      
  </div>
</section>

<?php include('../bottom.php') ?>
