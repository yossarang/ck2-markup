 <?php include('../head.php') ?>
 
<!----------------------------------------------------------------
   # 청깨상단배너
------------------------------------------------------------------>
<section id="onlyjesus_topbanner">
   <div class="topbanner_wrap">
      <img src="../img/onlyjesus/top_img1.png" alt="이미지" class="timg1"/>
      <img src="../img/onlyjesus/top_img2.png" alt="이미지" class="timg2"/>
      <div class="top_inner">
        <ul>
          <li>사랑은 여기 있으니 우리가 하나님을 사랑한 것이 아니요 하나님이 우리를 사랑하사 우리 죄를 속하기 위하여 화목제물로 그 아들을 보내셨음이라</li>
          <li>요한복음 1:15</li>
        </ul>
       </div> 
   </div>
</section>


<section id="location">
  <div class="location_wrap">
    <ul>
      <li><a href="/index.php"><img src="../img/intro/home_btn.png" alt="homebtn"></a></li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li>주만ZINE</li>
      <li><img src="../img/intro/location_arrow.png" alt="arrow"></li>
      <li class="nowpage">주만ZINE</li>
    </ul>
  </div>
</section>

<section id="onlyjesus">
  <div class="onlyjesus_wrap">
  
     <ul class="title">
       <li>주만ZINE</li>
       <li>ONLY JESUS</li>
     </ul>
  
      <div class="read_wrap">
        
    
         
         <ul class="title_wrap">
           <li>2017 사랑의교회 청년 국장단을 만나다</li>
           <li>2017.04.15 edited by  허윤정</li>
         </ul>
         
         
         <div class="content_wrap">
           이곳에 내용이 들어갑니다.
         </div>
         

          <div class="prev_wrap">
           <ul>
             <a href="">
             <li>주일찬양팀 그날의 기쁨 팀원모집</li>
             <li>PREV <img src="../img/dptnews/prev_arr.png" alt="아이콘"/></li>
             </a>
           </ul>
         </div>
         
         <div class="next_wrap">
           <ul>
             <a href="">
             <li>주일찬양팀 그날의 기쁨 팀원모집</li>
             <li>NEXT <img src="../img/dptnews/prev_arr.png" alt="아이콘"/></li>
             </a>
           </ul>
         </div>
         
         <div class="share_wrap">
             <ul class="share">
               <li><a href=""></a></li>
               <li><a href=""></a></li>
               <li><a href=""></a></li>
               <li><a href=""></a></li>
             </ul>
         </div>
         
         <a href="/onlyjesus/onlyjesus_list.php" class="bnt" >LIST</a>
         
         
        
         
      </div>
  </div>
</section>

<?php include('../bottom.php') ?>
