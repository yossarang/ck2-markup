 <?php include('head.php') ?>
 <!---------------------------------------------------------------
     # 메인배너
------------------------------------------------------------------>
 <section id="main_banner">
   <div class="banner_wrap">
   
   <img src="img/main/banner_arrow_left.png" alt="버튼" class="arrow_left"/>
   <img src="img/main/banner_arrow_right.png" alt="버튼" class="arrow_right"/>
   
   <!-- 메인배너1 -->
      <div class="banner bn1">
         <ul class="copy_rigt">
           <li>하나님 나라의 청년 제자운동</li>
           <li>
             <ul>
               <li><img src="img/main/stick_mainbanner.png" alt="슬러시"/><br /><span>복음주의 영성</span></li>
               <li><img src="img/main/stick_mainbanner.png" alt="슬러시"/><br /><span>일터에서의 소명자</span></li>
               <li><img src="img/main/stick_mainbanner.png" alt="슬러시"/><br /><span>세계를 품은 그리스도인</span></li>
             </ul>
           </li>
         </ul>
         <img src="img/main/main_banner1.jpg" alt="이미지" class="back"/>
      </div>
      
      <!-- 메인배너2 -->
      <div class="banner bn1">
         <img src="img/main/main_banner1.jpg" alt="이미지" class="back"/>
      </div>
      
      <!-- 메인배너3 -->
      <div class="banner bn1">
         <img src="img/main/main_banner1.jpg" alt="이미지" class="back"/>
      </div>
   </div>
 </section>
 
 
 
<!---------------------------------------------------------------
     # GODS WORDS
------------------------------------------------------------------>
 <section id="godwords">
  <div class="godwords_wrap">
    <div class="god_title">
      <ul>
        <li></li>
        <li>GOD'S</li>
        <li>WORDS</li>
      </ul>
    </div>
  
     <div class="line">
     </div>
  
    <div class="words_wrap">
      <ul class="words">
        <li>TODAY LETTER</li>
        <li>예레미야 20:13</li>
        <li>여호와께 노래하라 너희는 여호와를 찬양하라 가난한자의 생명을 행약자의 손에서 구원하셨음이라. </li>
      </ul>
      <div class="img_wrap">
        <img src="img/main/pot.jpg" alt="그림"/>
      </div>
    </div>
    
    <div class="date_wrap">
      <ul>
        <li><img src="img/main/word_arrow_left.jpg" /></li>
        <li>2017.03.25</li>
        <li><img src="img/main/word_arrow_right.jpg" /></li>
      </ul>
    </div> 
    
    
  </div>
 </section>
 
 
 
<!---------------------------------------------------------------
     # 설교말씀 (sermon)
------------------------------------------------------------------>
<section id="sermon">
  <div class="sermon_wrap">
    
    <div class="img_wrap">
      <img src="img/main/sermon_book.jpg" />
      <ul>
        <li>SERMON</li>
        <li>2017.01.13</li>
      </ul>
    </div>
    
    <div class="sermon_info">
      <ul>
        <li>베드로후서 3:18 &nbsp;&nbsp;&nbsp;&nbsp; 김승진 목사님</li>
        <li>"제자도 슈퍼 그리스도인들만의 것인가?"</li>
      </ul>
    </div>
    
  </div>
</section>
 
 
<!---------------------------------------------------------------
   # 국별소식
------------------------------------------------------------------> 
 <section id="yosnews">
  <div class="yosnews_wrap">
    <ul class="title">
      <li>국별소식</li>
      <li>YOS NEWS</li>
    </ul>
    
    <div class="seemore">
      <a href=""><span>MORE</span></a>
    </div>
    
    
  <div class="yosnews_innerwrap">   
    
    <!----------소식------------> 
    <div class="news_wrap">
    <a href="">
       <ul>
         <li class="title">
           <ul>
             <li>예배국1</li>
             <li>2017.03.27</li>
           </ul>
         </li>
         <li class="line"></li>
         <li>신년수련회 콰이어 섬김이 모집</li>
         <li>신년수련회 섬김을 통해 주님의 임재하심을 경험하고 삶의 기쁨이 회복되는 주일콰이어의 자리에 청년들을 모집합니다.</li>
       </ul>
       </a>
    </div>
  
  
  
  <!----------소식------------> 
    <div class="news_wrap">
    <a href="">
       <ul>
         <li class="title">
           <ul>
             <li>예배국2</li>
             <li>2017.03.27</li>
           </ul>
         </li>
         <li class="line"></li>
         <li>신년수련회 콰이어 섬김이 모집</li>
         <li>신년수련회 섬김을 통해 주님의 임재하심을 경험하고 삶의 기쁨이 회복되는 주일콰이어의 자리에 청년들을 모집합니다.</li>
       </ul>
       </a>
    </div>
 
  
  
  <!----------소식------------> 
    <div class="news_wrap">
    <a href="">
       <ul>
         <li class="title">
           <ul>
             <li>예배국3</li>
             <li>2017.03.27</li>
           </ul>
         </li>
         <li class="line"></li>
         <li>신년수련회 콰이어 섬김이 모집</li>
         <li>신년수련회 섬김을 통해 주님의 임재하심을 경험하고 삶의 기쁨이 회복되는 주일콰이어의 자리에 청년들을 모집합니다.</li>
       </ul>
       </a>
    </div>

  
  
  <!----------소식------------> 
    <div class="news_wrap">
    <a href="">
       <ul>
         <li class="title">
           <ul>
             <li>예배국4</li>
             <li>2017.03.27</li>
           </ul>
         </li>
         <li class="line"></li>
         <li>신년수련회 콰이어 섬김이 모집</li>
         <li>신년수련회 섬김을 통해 주님의 임재하심을 경험하고 삶의 기쁨이 회복되는 주일콰이어의 자리에 청년들을 모집합니다.</li>
       </ul>
       </a>
    </div>
  
   </div>
  
  </div>
 </section>
 
 
<!----------------------------------------------------------------
   # 청깨이슈
------------------------------------------------------------------> 
<section id="issue" >
<div class="issue_wrap">

<!---------- 비디오------------> 
  <div class="podcast_wrap">
    <ul class="pod_title">
      <li class="line"></li>
      <li class="title">HOLLY POD CAST</li>
    </ul>
    
    <iframe src="https://www.youtube.com/embed/SiAuF91kfJ8" frameborder="0" allowfullscreen class="movie"></iframe>
    
    <ul class="de_movie">
      <li>맥도날드 목사님의 설교영상</li>
      <li>2017.03.25</li>
    </ul>
  </div>
  
  
  <div class="device_line"></div>
 
<!---------- 주만지------------>  
  <div class="jesus_wrap">
    <div class="jesus_title">ONLY JESUS</div>
    
    <!---------- 주만지 소식1------------> 
    <div class="jnews1_wrap">
    
      <ul>
        <li><img src="img/main/jesus1.jpg" width="100%"></li>
        <li><span>국장단 인터뷰</span><span><img src="img/main/trian_jesus.jpg" width="20" height="17"></span></li>
        <li>"너무 은혜롭고 좋은 시간들이 많았던 것 같아요. 다음에도 또 하고싶어요"</li>
      </ul>
    </div>
    
    <!---------- 주만지 소식2------------>
    <div class="jnews2_wrap">
      <ul>
        <li><img src="img/main/jesus2.jpg"  width="100%"></li>
        <li><span>단선을 가다</span><span><img src="img/main/trian_jesus.jpg" width="20" height="17"></span></li>
        <li>“비가와도 너무 보람차고 즐겁고 좋은 시간이었던 것 같습니다.”
            손과 팔이 굽이 제대로 사용하지 못하시는 할머니와 할아버지들
            의 모습을 보면서 마음이 많이 아팠습니다. 하지만 그 분들이
            저희들 보다 훨씬 굳세고 밝은 모습으로 살고 계시다는 걸 
            금새 발견 할 수 있었죠. 
            
            이 분들은 나이가 80, 90이 넘으셔도 꿈을 간직하고 계셨습니다.
            그저 꿈만 꾸시는게 아니라 계획이 있고 그 계확아래 실천을 
            해가며 소망을 가꾸고 계셨습니다. 
            
            과연 우리 청년들은 언제부터 현실과 세상이란 벽에 막혀 꿈을
            잃고 소망을 잃고 살았는지 모르겠습니다. 누구보다 어렵고 
            척박한 환경에서도 꿈을 이어가는 어르신들의 모습에 잃어
            버렸던 열정과 꿈에대한 비젼을 되찾는 시간이 되었습니다.
        </li>
      </ul>
    </div>
  </div>
  
  
</div>  
</section>

<?php include('bottom.php') ?>
